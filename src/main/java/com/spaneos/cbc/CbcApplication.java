package com.spaneos.cbc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CbcApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CbcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Demo application ................");
	}
}
